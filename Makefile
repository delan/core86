.POSIX:

main.bin: main.s
	nasm -f bin -o $@ $?

main.360: main.bin
	cat $? /dev/zero | dd bs=512 count=720 > $@

main.vdi: main.bin
	qemu-img convert -O vdi $? $@

qemu: main.bin
	qemu-system-i386 -drive if=floppy,format=raw,file=main.bin

bochs: main.bin
	bochs -qf bochs -rc bochsrc

clean:
	rm -f main.bin main.map main.360 main.vdi

.PHONY: qemu bochs clean
