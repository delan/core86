[map brief main.map]
BITS 16
ORG 7C00h

load:
    push cs
    pop es                      ; sink section.core_text.start
    mov ax, 0202h               ; function 02h; two sectors
    mov cx, 0002h               ; cylinder 00h; sector 02h
    xor dh, dh                  ; head 00h
    mov bx, 7E00h               ; sink section.core_text.start
    int 13h                     ; optional: push stc int sti pop dx
    push ax
    call debug16
    push dx
    mov dx, 00E9h               ; for out
    mov al, `\n`                ; for out
    out dx, al
    pop dx
    jc load

    push ds
    pop es
    mov bp, sp

    call test_debugln
    call test_debugr
    call to_protected

    cli
    hlt

;;; pascal (value: word)
;;; stack <SP> [4] <BP> BP IP value
;;; Write $value in %04Xh format to port E9h.
debug16:
    enter 4, 0
    pusha
    pushf
    cld                         ; for stosb/outsb
    mov bx, DIGITS              ; for xlatb
    lea di, [bp-4]              ; for stosb
    mov si, di                  ; for outsb
    mov cx, 4                   ; for loop
    mov dx, [bp+4]              ; copy value

.loop:
    rol dx, 4
    mov al, dl
    and al, 0Fh
    xlatb
    ss stosb
    loop .loop

    mov dx, 00E9h               ; for outsb/out
    mov cx, 4
    rep ss outsb
    mov al, 'h'
    out dx, al

    popf
    popa
    leave
    ret 2

SECTION .data

DIGITS:
    db '0123456789ABCDEF'

SECTION .magic start=7DFEh

    dw 0xAA55

SECTION .core_text

;;; stack <SP> limit baseLo baseHi <BP> BP IP
;;; Switch to protected mode (SDM volume 3 § 9.9.1).
to_protected:
    enter 6, 0

    push DISABLING_INTERRUPTS.value
    push word [DISABLING_INTERRUPTS.len]
    call debug
    cli                         ; maskable interrupts
    in al, 70h                  ; for out
    or al, 80h                  ; for out
    out 70h, al                 ; non-maskable interrupts
    push OK.value
    push word [OK.len]
    call debugln

    push LOADING_GDT_GDTR.value
    push word [LOADING_GDT_GDTR.len]
    call debug
    mov word [bp-6], FLAT_GDT.limit
    mov dword [bp-4], FLAT_GDT.base
    lgdt [bp-6]
    push OK.value
    push word [OK.len]
    call debugln

    push SETTING_PE_CR0.value
    push word [SETTING_PE_CR0.len]
    call debug
    mov eax, cr0
    or ax, 1
    mov cr0, eax
    jmp FLAT_GDT.code_id:.continue
.continue:
    push OK.value
    push word [OK.len]
    call debugln

    push LOADING_DS_SS_ES_FS_GS.value
    push word [LOADING_DS_SS_ES_FS_GS.len]
    call debug
    mov ax, FLAT_GDT.data_id
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    push OK.value
    push word [OK.len]
    call debugln

    push LOADING_IDT_IDTR.value
    push word [LOADING_IDT_IDTR.len]
    call debug
    mov word [bp-6], MINIMAL_IDT.limit
    mov dword [bp-4], MINIMAL_IDT.base
    lidt [bp-6]
    push OK.value
    push word [OK.len]
    call debugln

    push ENABLING_INTERRUPTS.value
    push word [ENABLING_INTERRUPTS.len]
    call debug
    in al, 70h                  ; for out
    and al, 7Fh                 ; for out
    out 70h, al                 ; non-maskable interrupts
    sti                         ; maskable interrupts
    push OK.value
    push word [OK.len]
    call debugln

    mov al, '.'
.loop:
    loop .loop
    out 0xE9, al
    loop .loop

;;; #NP exception handler.
np_exception_handler:
    push NP_EXCEPTION.value
    push word [NP_EXCEPTION.len]
    call debugln

    add sp, 2                   ; discard error code
    iret

;;; pascal (value: near, len: word)
;;; stack <SP> <BP> BP IP len value
;;; Write $len bytes from DS:$value to port E9h.
debug:
    push bp
    mov bp, sp
    pusha
    pushf
    mov dx, 00E9h               ; for outsb
    mov si, [bp+6]              ; for outsb
    mov cx, [bp+4]              ; for outsb
    cld                         ; for outsb
    rep outsb
    popf
    popa
    leave
    ret 4

;;; pascal (value: near, len: word)
;;; stack <SP> <BP> BP IP len value
;;; Write $len bytes from DS:$value to port E9h, followed by LF.
debugln:
    push bp
    mov bp, sp
    push word [bp+6]            ; pass $value
    push word [bp+4]            ; pass $len
    call debug
    push ax
    mov al, `\n`
    out 0xE9, al
    pop ax
    leave
    ret 4

;;; pascal (ax, cx, dx, bx, sp, bp, si, di, flags: word)
;;; stack <SP> ax cx dx bx sp bp si di flags
;;;       <BP> BP DI SI DX CX AX IP flags di si bp sp bx dx cx ax
;;; Dump AX CX DX ... DI FLAGS to port E9h, followed by LF.
debugr:
    push ax
    push cx
    push dx
    push si
    push di
    enter 12h, 0
    lea si, [bp+1Eh]            ; for lodsw
    lea di, [bp-12h]            ; for stosw
    mov cx, 9                   ; for loop .swap

    pushf
.swap:
    std
    ss lodsw
    cld
    ss stosw
    loop .swap
    popf

    mov dx, 00E9h               ; for out
    mov cx, 9                   ; for loop .out

.out:
    call debug16
    mov al, ' '
    out dx, al
    loop .out

    mov al, `\n`
    out dx, al

    leave
    pop di
    pop si
    pop dx
    pop cx
    pop ax
    ret 12h

test_debugln:
    push DEBUGLN_TEST.value
    push word [DEBUGLN_TEST.len]
    call debugln
    ret

test_debugr:
    push DEBUGR_TEST.value
    push word [DEBUGR_TEST.len]
    call debugln

    pusha
    pushf
    call debugr

    pusha
    pushf

    mov ax, 0x1111
    mov cx, 0x2222
    mov dx, 0x3333
    mov bx, 0x4444
    mov si, 0x7777
    mov di, 0x8888

    pusha
    pushf
    call debugr

    stc
    pusha
    pushf
    call debugr

    std
    pusha
    pushf
    call debugr

    popf
    popa
    ret

SECTION .core_data

DEBUGLN_TEST:
.value:         db '• testing debugln'
.len:           dw $ - .value

DEBUGR_TEST:
.value:         db '• testing debugr'
.len:           dw $ - .value

OK:
.value:         db ' ok'
.len:           dw $ - .value

DISABLING_INTERRUPTS:
.value:         db '• disabling interrupts?'
.len:           dw $ - .value

LOADING_GDT_GDTR:
.value:         db '• loading GDT and GDTR?'
.len:           dw $ - .value

SETTING_PE_CR0:
.value:         db '• setting PE in CR0?'
.len:           dw $ - .value

LOADING_DS_SS_ES_FS_GS:
.value:         db '• loading DS/SS/ES/FS/GS?'
.len:           dw $ - .value

LOADING_IDT_IDTR:
.value:         db '• loading IDT and IDTR?'
.len:           dw $ - .value

ENABLING_INTERRUPTS:
.value:         db '• enabling interrupts?'
.len:           dw $ - .value

NP_EXCEPTION:
.value:         db '• #NP exception!'
.len:           dw $ - .value

;;; Basic Flat Model (SDM volume 3 §§ 3.2.1 3.4.5).
FLAT_GDT:
.base:
;;;                  ll       ll              bb              bb
;;;                  bb       PddStttt        GDLAllll        bb
.null_id:       equ $ - .base
.null:          db 0x00,    0x00,           0x00,           0x00
                db 0x00,    0b00000000,     0b00000000,     0x00
.code_id:       equ $ - .base
.code:          db 0xFF,    0xFF,           0x00,           0x00
                db 0x00,    0b10011010,     0b10001111,     0x00
.data_id:       equ $ - .base
.data:          db 0xFF,    0xFF,           0x00,           0x00
                db 0x00,    0b10010010,     0b10001111,     0x00
.vgat_id:       equ $ - .base
.vgat:          db 0x00,    0x00,           0x00,           0x80
                db 0x0B,    0b10010010,     0b10000000,     0x00
.limit:         dw $ - .base - 1

;;; Minimal Interrupt Descriptor Table (SDM volume 3 § 6.10).
;;; The only gate descriptor we absolutely must define is the one for
;;; #NP, because it will be raised when any other interrupt occurs.
MINIMAL_IDT:
.base:
;;;                  oooo    ssss    Pdd0D110000!!!!!    oooo
._de_id:        equ $ - .base
._de:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._db_id:        equ $ - .base
._db:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
.nmi_id:        equ $ - .base
.nmi:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._bp_id:        equ $ - .base
._bp:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._of_id:        equ $ - .base
._of:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._br_id:        equ $ - .base
._br:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._ud_id:        equ $ - .base
._ud:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._nm_id:        equ $ - .base
._nm:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._df_id:        equ $ - .base
._df:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
.cso_id:        equ $ - .base
.cso:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._ts_id:        equ $ - .base
._ts:           dw 0x0000, 0x0000, 0b0000011000000000, 0x0000
._np_id:        equ $ - .base
._np:           dw np_exception_handler, FLAT_GDT.code_id
                dw                 0b1000011000000000, 0x0000
.limit:         dw $ - .base - 1
