#!/usr/bin/env perl

use v5.24;
use strict;
use warnings;

sub option {
    my $args = shift;
    my $option = shift;

    @_ = map sprintf('0x%X', $_), @_;
    $_ = join ',', @_;

    push @$args, $option, $_;
}

sub heading {
    my $source = shift;
    my $heading = shift;

    while (<$source>) {
        last if /^-- \Q$heading\E --+$/;
    }

    if (eof) {
        die "fatal: could not find heading “$heading” in map file"
    }

    <$source>; # skip blank line
}

if ($#ARGV + 1 < 3) {
    say STDERR "usage:  $0 <image.bin> <image.map> <section> [section] ...";
    say STDERR "  e.g.  $0 foo.bin foo.map text bar baz";
    say STDERR '';
    say STDERR '     •  assemble image.bin with nasm -f bin -o image.bin ...';
    say STDERR '     •  in source: [map (all|brief|sections|segments) image.map]';
    exit 1;
}

# ndisasm doesn’t support --
my @args = ('ndisasm', shift);
my $map_path = shift;
my @sections = @ARGV;
my $start;
my $stop;

open my $map, '<', $map_path
    or die "$map_path: $!";

heading $map, 'Program origin';
chomp($start = <$map>);
$start = hex $start;
option \@args, '-o', $start;

heading $map, 'Sections (summary)';
<$map>; # skip column headers

while (<$map>) {
    last if /^$/;
    chomp;

    my @line = split;
    my $next = hex $line[1];
    $stop = hex $line[2];

    my @sections = map quotemeta, @sections;
    my $sections = join '|', @sections;

    if (/ [.]($sections)$/) {
        option \@args, '-k', $start, $next - $start;
        $start = $stop;
    }
}

option \@args, '-k', $start, $stop - $start;
say STDERR join(' ', @args);
exec { $args[0] } @args;
